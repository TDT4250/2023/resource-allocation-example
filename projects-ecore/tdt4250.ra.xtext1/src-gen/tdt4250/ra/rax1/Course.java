/**
 * generated by Xtext 2.18.0.M3
 */
package tdt4250.ra.rax1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.ra.rax1.Course#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.ra.rax1.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.ra.rax1.Course#getAllocations <em>Allocations</em>}</li>
 * </ul>
 *
 * @see tdt4250.ra.rax1.Rax1Package#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject
{
  /**
   * Returns the value of the '<em><b>Code</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Code</em>' attribute.
   * @see #setCode(String)
   * @see tdt4250.ra.rax1.Rax1Package#getCourse_Code()
   * @model
   * @generated
   */
  String getCode();

  /**
   * Sets the value of the '{@link tdt4250.ra.rax1.Course#getCode <em>Code</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Code</em>' attribute.
   * @see #getCode()
   * @generated
   */
  void setCode(String value);

  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see tdt4250.ra.rax1.Rax1Package#getCourse_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link tdt4250.ra.rax1.Course#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Allocations</b></em>' containment reference list.
   * The list contents are of type {@link tdt4250.ra.rax1.ResourceAllocation}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Allocations</em>' containment reference list.
   * @see tdt4250.ra.rax1.Rax1Package#getCourse_Allocations()
   * @model containment="true"
   * @generated
   */
  EList<ResourceAllocation> getAllocations();

} // Course
