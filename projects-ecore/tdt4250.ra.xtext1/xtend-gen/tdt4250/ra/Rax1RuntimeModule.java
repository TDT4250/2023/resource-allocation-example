/**
 * generated by Xtext 2.18.0.M3
 */
package tdt4250.ra;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
@SuppressWarnings("all")
public class Rax1RuntimeModule extends AbstractRax1RuntimeModule {
}
